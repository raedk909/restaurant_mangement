// import 'dart:convert';
//
// import 'package:gestion_restaurantion/Models/sous_Categorie.dart';
// import 'package:get/get.dart';
// import 'package:http/http.dart' as http;
//
//
// class SousCategorieController extends GetxController {
//   final _sousCategories = <SousCategories>[].obs;
//   List<SousCategories> get sousCategories => _sousCategories;
//
//   @override
//   void onInit() {
//     super.onInit();
//     fetchSousCategories();
//   }
//
//   Future<void> fetchSousCategories() async {
//     final response = await http.get(Uri.parse('http://localhost:9090/api/souscategorie'));
//     if (response.statusCode == 200) {
//       final List<dynamic> responseData = json.decode(response.body);
//       final List<SousCategories> loadedSousCategories = responseData
//           .map((sousCategoryData) => SousCategories.fromJson(sousCategoryData))
//           .toList();
//       _sousCategories.assignAll(loadedSousCategories);
//     } else {
//       // Handle error
//       print('Failed to fetch sous categories: ${response.statusCode}');
//     }
//   }
//
//   Future<void> addSousCategorie(SousCategories sousCategorie) async {
//     final response = await http.post(
//       Uri.parse('http://localhost:9090/api/souscategorie'),
//       headers: {'Content-Type': 'application/json'},
//       body: json.encode(sousCategorie.toJson()),
//     );
//     if (response.statusCode == 201) {
//       final newSousCategorie = SousCategories.fromJson(json.decode(response.body));
//       _sousCategories.add(newSousCategorie);
//     } else {
//       // Handle error
//       print('Failed to add sous categorie: ${response.statusCode}');
//     }
//   }
//
//   Future<void> updateSousCategorie(SousCategories sousCategorie) async {
//     final response = await http.put(
//       Uri.parse('http://localhost:9090/api/souscategorie/${sousCategorie.idSousCategorie}'),
//       headers: {'Content-Type': 'application/json'},
//       body: json.encode(sousCategorie.toJson()),
//     );
//     if (response.statusCode == 200) {
//       final updatedSousCategorie = SousCategories.fromJson(json.decode(response.body));
//       final index = _sousCategories.indexWhere((sc) => sc.idSousCategorie == updatedSousCategorie.idSousCategorie);
//       _sousCategories[index] = updatedSousCategorie;
//     } else {
//       // Handle error
//       print('Failed to update sous categorie: ${response.statusCode}');
//     }
//   }
//
//   Future<void> deleteSousCategorie(int idSousCategorie) async {
//     final response = await http.delete(Uri.parse('http://localhost:9090/api/souscategorie/$idSousCategorie'));
//     if (response.statusCode == 204) {
//       _sousCategories.removeWhere((sousCategorie) => sousCategorie.idSousCategorie == idSousCategorie);
//     } else {
//       // Handle error
//       print('Failed to delete sous categorie: ${response.statusCode}');
//     }
//   }
// }