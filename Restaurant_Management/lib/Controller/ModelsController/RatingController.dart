import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gestion_restaurantion/Models/Article.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class RatingController extends GetxController {
  final _ratings = RxList<Rating>([]);
  RxList<Rating> get ratings => _ratings;

  final storage = FlutterSecureStorage();

  Future<Rating?> addRating(Rating rating) async {
    final token = await storage.read(key: "jwt_token");
    final response = await http.post(
      Uri.parse('http://192.168.43.117:9090/api/rating'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(rating.toJson()),
    );
    if (response.statusCode == 201) {
      final newRating = Rating.fromJson(json.decode(response.body));
      _ratings.add(newRating);
      return newRating; // Return the new rating object
    } else {
      // Handle error
      print('Failed to add rating. Status code: ${response.statusCode}. Error: ${response.body}');
      return null; // Return null to indicate failure
    }
  }
}
