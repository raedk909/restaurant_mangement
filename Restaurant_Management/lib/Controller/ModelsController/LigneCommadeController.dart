import 'dart:convert';

import 'package:gestion_restaurantion/Models/Article.dart';
import 'package:gestion_restaurantion/Models/ligneCommande.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ShoppingCartController extends GetxController {
  RxList<ligneCommande> cartItems = <ligneCommande>[].obs;

  RxDouble totalPrice = RxDouble(0.0);

  final storage = FlutterSecureStorage();

  void addItem(Articles article, int quantity) {
    final itemIndex = cartItems.indexWhere((item) => item.article.idArticle == article.idArticle);
    if (itemIndex >= 0) {
      final existingItem = cartItems[itemIndex];
      existingItem.quantite += quantity;
      existingItem.prixLigne = (existingItem.quantite * existingItem.article.prix);
    } else {
      final newItem = ligneCommande(
        article: article,
        quantite: quantity,
        prixLigne: article.prix * quantity.toDouble() ,
        idLigneCommande: DateTime.now().millisecondsSinceEpoch,
      );
      cartItems.add(newItem);
    }
    totalPrice.value = cartItems.fold(0, (previousValue, item) => previousValue + item.prixLigne);
  }

  void removeItem(ligneCommande item) {
    cartItems.remove(item);
    totalPrice.value -= item.prixLigne;
  }

  int getItemCount() {
    return cartItems.length;
  }

  double getTotalCost() {
    return totalPrice.value;
  }

  List<ligneCommande> getAllItems() {
    return cartItems.toList();
  }

  void clearCart() {
    cartItems.clear();
    totalPrice.value = 0.0;
  }

  void incrementQuantity(ligneCommande item) {
    final oldQuantity = item.quantite;
    item.quantite++;
    item.prixLigne = (item.quantite * item.article.prix) as double;
    final newQuantity = item.quantite;
    totalPrice.value += (newQuantity - oldQuantity) * item.article.prix;
    cartItems.refresh();
  }

  void decrementQuantity(ligneCommande item) {
    final oldQuantity = item.quantite;
    if (oldQuantity > 1) {
      item.quantite--;
      item.prixLigne = (item.quantite * item.article.prix) as double;
      final newQuantity = item.quantite;
      totalPrice.value += (newQuantity - oldQuantity) * item.article.prix;
    } else {
      removeItem(item);
    }
    cartItems.refresh();
  }
  Future <void> saveCartToDatabase(List<ligneCommande> cartItems) async {
    final token = await storage.read(key: "jwt_token");
    final cartData = cartItems.map((item) => item.toJson()).toList();
    final body = json.encode({'ligneCommande': cartData});
    final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/ligneCommandes/saveAll'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: body
    );
    if (response.statusCode == 200) {
      // Successful response
      print('Cart saved to database');
    } else {
      // Handle error
      print('Err: ${response.reasonPhrase}');
    }
  }
  Future <ligneCommande?> saveCartItemToDatabase(ligneCommande item) async {
    final token = await storage.read(key: "jwt_token");
    final body = json.encode(item.toJson());
    final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/ligneCommandes'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: body
    );
    if (response.statusCode == 201) {
      // Successful response
      print('Cart item saved to database');
      //print(json.decode(response.body));
      return ligneCommande.fromJson(json.decode(response.body));
    } else {
      // Handle error
      print('Err: ${response.reasonPhrase}');
      return null;
    }
  }
  Future<ligneCommande?> getSavedCartItemFromDatabase(int id) async {
    final token = await storage.read(key: "jwt_token");
    final response = await http.get(
      Uri.parse('http://192.168.43.117:9090/api/ligneCommandes/$id'),
      headers: {
        'Content-Type': 'application/json;charset=UTF-8', // set the content type to json and encoding to utf-8
        'Authorization': 'Bearer $token',
        'Accept': 'application/json;charset=UTF-8', // set the accept header to json and encoding to utf-8
      },
    );
    if (response.statusCode == 200) {
      // Successful response
      final jsonData = json.decode(response.body);
      final ligneCommandeData = jsonData['ligneCommande'];
      final savedItem = ligneCommande.fromJson(ligneCommandeData);
      print('Cart item retrieved from database');
      return savedItem;
    } else {
      // Handle error
      print('Err12345: ${response.reasonPhrase}');
      return null;
    }
  }
  Future<void> updateLigneCommandeQuantite(int id, int quantite) async {
    final token = await storage.read(key: "jwt_token");
    final response = await http.put(
      Uri.parse('http://192.168.43.117:9090/api/ligneCommandes/quantite/$id?quantite=$quantite'),
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Bearer $token',
        'Accept': 'application/json;charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      // Handle success
      print("Success");
      update();
    } else if (response.statusCode == 400) {
      // Handle 400 error
      print('Error updating ligne commande quantite: Bad request');
    } else if (response.statusCode == 404) {
      // Handle 404 error
      print('Error updating ligne commande quantite: Not found');
    } else if (response.statusCode == 500) {
      // Handle 500 error
      print('Error updating ligne commande quantite: Internal server error');
    } else {
      // Handle other errors
      print('Error updating ligne commande quantite: ${response.reasonPhrase}');
    }
  }

  Future<void> deleteCartItemFromDatabase(int id) async {
    final token = await storage.read(key: "jwt_token");
    final response = await http.delete(
        Uri.parse('http://192.168.43.117:9090/api/ligneCommandes/$id'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        }

    );
    if (response.statusCode == 204) {
      // Successful response
      print('Cart item deleted from database');

    } else {
      // Handle error
      print('Err: ${response.reasonPhrase}');
    }
  }

  }
