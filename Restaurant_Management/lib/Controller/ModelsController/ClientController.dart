import 'dart:convert';

import 'package:gestion_restaurantion/Models/Client.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ClientController extends GetxController {
  final _clients = RxList<Client>([]);

  RxList<Client> get clients => _clients;

  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchClients();
  }

  Future<Map<String, String>> getHeaders() async {
    final token = await storage.read(key: "jwt_token");
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }

  Future<void> fetchClients() async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/clients'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        _clients.clear();
        _clients.addAll(jsonList.map((model) => Client.fromJson(model)));
      } else {
        throw Exception('Failed to load clients');
      }
    } catch (e) {
      print('Failed to fetch clients: $e');
    }
  }

  Future<void> updateClient(int idClient, Client updatedClient) async {
    final response = await http.put(
        Uri.parse('http://192.168.43.117:9090/api/clients/$idClient'),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(updatedClient.toJson()));
    if (response.statusCode == 200) {
      final index = _clients.indexWhere((client) =>
      client.id == idClient);
      if (index != -1) {
        _clients[index] = updatedClient;
      }
    } else {
      // Handle error
      print('Failed to update client: ${response.statusCode}');
    }
  }

  Future<Client> getOneClient(int idClient) async {
    final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/clients/$idClient'));
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      final Client client = Client.fromJson(responseData);
      return client;
    } else {
      // Handle error
      print('Failed to get client: ${response.statusCode}');
      throw Exception('Failed to get client');
    }
  }
  Future<Client> getClientByUser(int userId) async {
    final token = await storage.read(key: "jwt_token");
    final response = await http.get(
      Uri.parse('http://192.168.43.117:9090/api/clients/user?userId=$userId'),
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Bearer $token',
        'Accept': 'application/json;charset=UTF-8'
      },
    );
    if (response.statusCode == 200) {
      try {
        final Map<String, dynamic> responseData = json.decode(response.body);
        final Client client = Client.fromJson(responseData);
        return client;
      } catch (error) {
        print('Failed to decode JSON data: $error');
        throw Exception('Failed to get client by user');
      }
    } else {
      // Handle error
      print('Failed to get client by user: ${response.statusCode}');
      throw Exception('Failed to get client by user');
    }
  }
}