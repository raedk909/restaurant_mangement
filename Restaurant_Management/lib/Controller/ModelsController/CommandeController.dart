import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gestion_restaurantion/Models/Commande.dart';
import 'package:intl/intl.dart';

class CommandeController extends GetxController {

  final _commandes = RxList<Commande>([]);
  RxList<Commande> get commandes => _commandes;

  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchCommandes();
  }

  Future<Map<String, String>> getHeaders() async {
    final token = await storage.read(key: "jwt_token");
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }

  Future<void> findCommandesByDate(DateTime date) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/commandes/ByDate/${DateFormat('yyyy-MM-dd').format(date)}'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        if (jsonList == null) {
          throw Exception('Empty response');
        }
        _commandes.clear();
        _commandes.addAll(jsonList.map((model) => Commande.fromJson(model)));
      } else {
        throw Exception('Failed to load commandes by date, status code: ${response.statusCode}');
      }
    } on SocketException catch (e) {
      throw Exception('Failed to fetch commandes by date, SocketException: $e');
    } on TimeoutException catch (e) {
      throw Exception('Failed to fetch commandes by date, TimeoutException: $e');
    } on FormatException catch (e) {
      throw Exception('Failed to fetch commandes by date, FormatException: $e');
    } catch (e) {
      throw Exception('Failed to fetch commandes by date: $e');
    }
  }

  Future<void> saveCommande(Commande commande) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/commandes'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode(commande.toJson()),
      );
      if (response.statusCode == 201) {
        // Successful response
        print('Commande saved to database');
      } else if (response.statusCode == 400) {
        final errors = json.decode(response.body);
        print('Failed to save commande due to validation errors: $errors');
        // Log the error details
        print('Validation errors details:');
        for (final key in errors.keys) {
          final errorMessage = errors[key];
          print('$key: $errorMessage');
        }
      } else if (response.statusCode == 401) {
        // Handle authentication errors
        print('Failed to save commande due to authentication error');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        // Handle server errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to save commande due to server error. Reason: $errorMessage');
      } else {
        // Handle other errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to save commande. Reason: $errorMessage');
      }
    } on SocketException catch (e) {
      // Handle network error
      print('Failed to save commande due to network error. Reason: $e');
    } on TimeoutException catch (e) {
      // Handle timeout error
      print('Failed to save commande due to timeout error. Reason: $e');
    } on Exception catch (e) {
      // Handle other exceptions
      print('Failed to save commande due to unexpected error. Reason: $e');
    }
  }
  Future<void> fetchCommandes() async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/commandes'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        _commandes.clear();
        _commandes.addAll(jsonList.map((model) => Commande.fromJson(model)));
      } else {
        throw Exception('Failed to load commandes');
      }
    } catch (e) {
      print('Failed to fetch commandes: $e');
    }

  }

  Future<void> updateCommande(Commande commande) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.put(
        Uri.parse('http://192.168.43.117:9090/api/commandes/${commande.idCommande}'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode(commande.toJson()),
      );
      if (response.statusCode == 200) {
        // Successful response
        print('Commande updated in database');
      } else if (response.statusCode == 400) {
        final errors = json.decode(response.body);
        print('Failed to update commande due to validation errors: $errors');
        // Log the error details
        print('Validation errors details:');
        for (final key in errors.keys) {
          final errorMessage = errors[key];
          print('$key: $errorMessage');
        }
      } else if (response.statusCode == 401) {
        // Handle authentication errors
        print('Failed to update commande due to authentication error');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        // Handle server errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to update commande due to server error. Reason: $errorMessage');
      } else {
        // Handle other errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to update commande. Reason: $errorMessage');
      }
    } on SocketException catch (e) {
      // Handle network error
      print('Failed to update commande due to network error. Reason: $e');
    } on TimeoutException catch (e) {
      // Handle timeout error
      print('Failed to update commande due to timeout error. Reason: $e');
    } on Exception catch (e) {
      // Handle other exceptions
      print('Failed to update commande due to unexpected error. Reason: $e');
    }
  }

  Future<void> deleteCommande(int id) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.delete(
        Uri.parse('http://192.168.43.117:9090/api/commandes/$id'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 204) {
        // Successful response
        print('Commande deleted from database');
        // Remove the deleted Commande from the _commandes list
        _commandes.removeWhere((c) => c.idCommande == id);
      } else if (response.statusCode == 401) {
        // Handle authentication errors
        print('Failed to delete commande due to authentication error');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        // Handle server errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to delete commande due to server error. Reason: $errorMessage');
      } else {
        // Handle other errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to delete commande. Reason: $errorMessage');
      }
    } on SocketException catch (e) {
      // Handle network error
      print('Failed to delete commande due to network error. Reason: $e');
    } on TimeoutException catch (e) {
      // Handle timeout error
      print('Failed to delete commande due to timeout error. Reason: $e');
    } on Exception catch (e) {
      // Handle other exceptions
      print('Failed to delete commande due to unexpected error. Reason: $e');
    }
  }
  Future<Commande> getCommande(int id) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/commandes/$id'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200 && response.body != null) {
        final jsonModel = json.decode(response.body);
        final commande = Commande.fromJson(jsonModel);
        return commande;
      } else {
        throw Exception('Failed to load commande with id $id');
      }
    } catch (e) {
      print('Failed to fetch commande with id $id: $e');
      rethrow;
    }
  }

  Future<void> setCommandeValide(int idCommande) async {
    final commande = await getCommande(idCommande);
    if (commande == null) {
      throw Exception('Commande not found');
    }
    // Set the etat to valide.
    commande.etat = Etat.valide;
    // Save the updated commande.
    await updateCommande(commande);
  }

  Future<void> setCommandeAnnule(int idCommande) async {
    final commande = await getCommande(idCommande);
    if (commande == null) {
      throw Exception('Commande not found');
    }
    // Set the etat to annule.
    commande.etat = Etat.annule;
    // Save the updated commande.
    await updateCommande(commande);
  }

  Future<void> updateCommandeDescription(int idCommande , String description) async {
    final commande = await getCommande(idCommande);
    if (commande == null) {
      throw Exception('Commande not found');
    }
    // Set the etat to annule.
    commande.description = description;
    // Save the updated commande.
    await updateCommande(commande);
  }

}