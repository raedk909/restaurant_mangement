import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gestion_restaurantion/Models/TableRestaurant.dart';

class TableRestaurantController extends GetxController {
  final _tableRestaurants = RxList<TableRestaurant>([]);
  RxList<TableRestaurant> get tableRestaurants => _tableRestaurants;

  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchTableRestaurants();
  }

  Future<Map<String, String>> getHeaders() async {
    final token = await storage.read(key: "jwt_token");
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }

  Future<void> saveTableRestaurant(TableRestaurant tableRestaurant) async {
    final headers = await getHeaders();
    final body = jsonEncode(tableRestaurant.toJson());
    try {
      final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/tables'),
        headers: headers,
        body: body,
      );
      if (response.statusCode == 201) {
        final newTableRestaurant = TableRestaurant.fromJson(
            jsonDecode(response.body));
        _tableRestaurants.add(newTableRestaurant);

      } else {
        throw Exception('Failed to save the TableRestaurant');
      }
    } catch (e) {
      print('Failed to save TableRestaurant: $e');
    }
  }

  Future<TableRestaurant?> getTableByName(int numeroTable) async {
    final headers = await getHeaders();
    try {
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/tables/numero/$numeroTable'),
        headers: headers,
      );
      if (response.statusCode == 200) {
        final tableJson = jsonDecode(response.body);
        final table = TableRestaurant.fromJson(tableJson);
        print("from the controller : $table");
        return table;
      } else {
        throw Exception('Failed to get the TableRestaurant by numero');
      }
    } catch (e) {
      print('Failed to get TableRestaurant by numero: $e');
      return null;
    }
  }

  Future<void> fetchTableRestaurants() async {
    try {
      final headers = await getHeaders();
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/tables'),
        headers: headers,
      );
      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        _tableRestaurants.clear();
        _tableRestaurants.addAll(
            jsonList.map((model) => TableRestaurant.fromJson(model)));
        print(tableRestaurants.length);
      } else {
        throw Exception('Failed to fetch the TableRestaurants');
      }
    } catch (e) {
      print('Failed to fetch TableRestaurants: $e');
    }
  }
}