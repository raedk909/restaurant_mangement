import 'dart:convert';

import 'package:gestion_restaurantion/Models/User.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserController extends GetxController {
  final _users = RxList<User>([]);

  RxList<User> get users => _users;

  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchUsers();
  }

  Future<Map<String, String>> getHeaders() async {
    final token = await storage.read(key: "jwt_token");
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }

  Future<void> fetchUsers() async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/users'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        _users.clear();
        _users.addAll(jsonList.map((model) => User.fromJson(model)));
      } else {
        throw Exception('Failed to load users');
      }
    } catch (e) {
      print('Failed to fetch users: $e');
    }
  }

  Future<void> updateUser(int idUser, User updatedUser) async {
    final response = await http.put(
        Uri.parse('http://192.168.43.117:9090/api/users/$idUser'),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(updatedUser.toJson()));
    if (response.statusCode == 200) {
      final index = _users.indexWhere((user) => user.idUser == idUser);
      if (index != -1) {
        _users[index] = updatedUser;
      }
    } else {
      // Handle error
      print('Failed to update user: ${response.statusCode}');
    }
  }

  Future<User> getOneUser(int idUser) async {
    final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/users/$idUser'));
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      final User user = User.fromJson(responseData);
      return user;
    } else {
      // Handle error
      print('Failed to get user: ${response.statusCode}');
      throw Exception('Failed to get user');
    }
  }
  Future<User> getUserByEmail(String email) async {
    final token = await storage.read(key: "jwt_token");
    final response = await http.get(
      Uri.parse('http://192.168.43.117:9090/api/users/email/$email'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      final User user = User.fromJson(responseData);
      return user;
    } else {
      // Handle error
      print('Failed to get user by email: ${response.statusCode}');
      throw Exception('Failed to get user by email');
    }
  }
  Future<User?> get loggedInUser async {
    try {
      final token = await storage.read(key: 'jwt_token');
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/users/me'),
        headers: {'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        final User user = User.fromJson(responseData);
        return user;
      } else {
        // Handle errors
        return null;
      }
    } catch (e) {
      // Handle network errors
      return null;
    }
  }
}


  // }
  // Future<User?> getUserByEmail(String email) async {
  //   try {
  //     final token = await storage.read(key: "jwt_token");
  //     final response = await http.get(
  //       Uri.parse('http://192.168.43.117:9090/api/users/email/$email'),
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Authorization': 'Bearer $token',
  //       },
  //     );
  //     if (response.statusCode == 200 && response.body != null) {
  //       final Iterable jsonList = json.decode(response.body);
  //       final List<User> users = jsonList.map((model) => User.fromJson(model)).toList();
  //       final user = users.firstWhere((user) => user.email == email);
  //       return user;
  //     } else {
  //       throw Exception('Failed to load users');
  //     }
  //   } catch (e) {
  //     print('Failed to fetch users: $e');
  //     return null;
  //   }
  // }

