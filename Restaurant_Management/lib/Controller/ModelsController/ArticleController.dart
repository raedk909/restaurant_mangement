import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gestion_restaurantion/Models/Article.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../AuthenticationController/auth_Controller.dart';

class ArticleController extends GetxController {
  final _articles = RxList<Articles>([]); // Change here
  RxList<Articles> get articles => _articles; // Change here


  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchArticles();
  }

  Future<void> fetchArticles() async {
    try {
      final token = await storage.read(key: "jwt_token");
      final url = 'http://192.168.43.117:9090/api/articles';
      final encodedUrl = Uri.encodeFull(url);
      final response = await http.get(
        Uri.parse(encodedUrl),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Authorization': 'Bearer $token',
          'Accept': 'application/json;charset=UTF-8',
        },
      );
      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        _articles.clear();
        _articles.addAll(jsonList.map((model) => Articles.fromJson(model)));
      } else {
        throw Exception('Failed to load articles');
      }
    } catch (e) {
      print('Failed to fetch articles: $e');
    }
  }
  Future<void> updateArticle(Articles article) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.put(
        Uri.parse('http://192.168.43.117:9090/api/articles/${article.idArticle}'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode(article.toJson()),
      );
      if (response.statusCode == 200) {
        // Successful response
        print('Article updated in database');
      } else if (response.statusCode == 400) {
        final errors = json.decode(response.body);
        print('Failed to update article due to validation errors: $errors');
        // Log the error details
        print('Validation errors details:');
        for (final key in errors.keys) {
          final errorMessage = errors[key];
          print('$key: $errorMessage');
        }
      } else if (response.statusCode == 401) {
        // Handle authentication errors
        print('Failed to update article due to authentication error');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        // Handle server errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to update article due to server error. Reason: $errorMessage');
      } else {
        // Handle other errors
        final errorMessage = response.reasonPhrase ?? 'Unknown error';
        print('Failed to update article. Reason: $errorMessage');
      }
    } on SocketException catch (e) {
      // Handle network error
      print('Failed to update article due to network error. Reason: $e');
    } on TimeoutException catch (e) {
      // Handle timeout error
      print('Failed to update article due to timeout error. Reason: $e');
    } on Exception catch (e) {
      // Handle other exceptions
      print('Failed to update article due to unexpected error. Reason: $e');
    }
  }


  Future<Articles> getOneArticle(int idArticle) async {
    final response = await http
        .get(Uri.parse('http://192.168.43.117:9090:9090/api/articles/$idArticle'));
    if (response.statusCode == 200) {
      final Map<String, dynamic> jsonData = json.decode(response.body);
      return Articles.fromJson(jsonData);
    } else {
      throw Exception('Failed to load article with id $idArticle');
    }
  }

  // double calculateAverageRating(List<Rating> ratings) {
  //   if (ratings.isEmpty) {
  //     return 0.0;
  //   }
  //   double sum = 0.0;
  //   for (final rating in ratings) {
  //     sum += rating.value;
  //   }
  //   return sum / ratings.length;
  // }

  double calculateAverageRating(Articles article) {
    final ratings = article.ratings;
    if (ratings.isEmpty) {
      return 0.0;
    }
    final sum = ratings.fold(0.0, (acc, rating) => acc + rating.value);
    return sum / ratings.length;
  }
}
