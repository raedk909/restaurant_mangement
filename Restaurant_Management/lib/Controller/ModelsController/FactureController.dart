import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gestion_restaurantion/Models/Commande.dart';
import 'package:gestion_restaurantion/Models/Facture.dart';
import 'package:intl/intl.dart';

class FactureController extends GetxController {
  final _factures = RxList<Facture>([]);

  RxList<Facture> get factures => _factures;

  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchFactures();
  }

  Future<Map<String, String>> getHeaders() async {
    final token = await storage.read(key: "jwt_token");
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }

  Future<void> findFacturesByDate(DateTime date) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/factures/ByDate/${DateFormat(
            'yyyy-MM-dd').format(date)}'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        if (jsonList == null) {
          throw Exception('Empty response');
        }
        _factures.clear();
        _factures.addAll(jsonList.map((model) => Facture.fromJson(model)));
      } else {
        throw Exception(
            'Failed to load factures by date, status code: ${response
                .statusCode}');
      }
    } on SocketException catch (e) {
      throw Exception('Failed to fetch factures by date, SocketException: $e');
    } on TimeoutException catch (e) {
      throw Exception('Failed to fetch factures by date, TimeoutException: $e');
    } on FormatException catch (e) {
      throw Exception('Failed to fetch factures by date, FormatException: $e');
    } catch (e) {
      throw Exception('Failed to fetch factures by date: $e');
    }
  }

  Future<Facture> saveFacture(Facture facture) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/factures'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode(facture.toJson()),
      );

      if (response.statusCode == 201) {
        final newFacture = Facture.fromJson(json.decode(response.body));
        _factures.add(newFacture);
        return newFacture;
      } else {
        throw Exception(
            'Failed to save facture, status code: ${response.statusCode}');
      }
    } on SocketException catch (e) {
      throw Exception('Failed to save facture, SocketException: $e');
    } on TimeoutException catch (e) {
      throw Exception('Failed to save facture, TimeoutException: $e');
    } on FormatException catch (e) {
      throw Exception('Failed to save facture, FormatException: $e');
    } catch (e) {
      throw Exception('Failed to save facture: $e');
    }
  }

  Future<void> fetchFactures() async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/factures'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(response.body);
        if (jsonList == null) {
          throw Exception('Empty response');
        }
        _factures.clear();
        _factures.addAll(jsonList.map((model) => Facture.fromJson(model)));
      } else {
        throw Exception('Failed to load factures, status code: ${response.statusCode}');
      }
    } on SocketException catch (e) {
      throw Exception('Failed to fetch factures, SocketException: $e');
    } on TimeoutException catch (e) {
      throw Exception('Failed to fetch factures, TimeoutException: $e');
    } on FormatException catch (e) {
      throw Exception('Failed to fetch factures, FormatException: $e');
    } catch (e) {
      throw Exception('Failed to fetch factures: $e');
    }
  }
  Future<Facture> getFacture(int factureId) async {
    try {
      final token = await storage.read(key: "jwt_token");
      final response = await http.get(
        Uri.parse('http://192.168.43.117:9090/api/factures/$factureId'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200 && response.body != null) {
        final jsonFacture = json.decode(response.body);
        return Facture.fromJson(jsonFacture);
      } else {
        throw Exception('Failed to load facture, status code: ${response.statusCode}');
      }
    } on SocketException catch (e) {
      throw Exception('Failed to fetch facture, SocketException: $e');
    } on TimeoutException catch (e) {
      throw Exception('Failed to fetch facture, TimeoutException: $e');
    } on FormatException catch (e) {
      throw Exception('Failed to fetch facture, FormatException: $e');
    } catch (e) {
      throw Exception('Failed to fetch facture: $e');
    }
  }

}
