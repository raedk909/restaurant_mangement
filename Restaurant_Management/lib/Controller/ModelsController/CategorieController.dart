import 'dart:convert';

import 'package:gestion_restaurantion/Models/Catgorie.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class CategorieController extends GetxController {
  final _categories = RxList<Categorie>([]); // Change here
  RxList<Categorie> get categories => _categories; // Change here

  final storage = FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    fetchCategories();
  }
  Future<Map<String, String>> getHeaders() async {
    final token = await storage.read(key: "jwt_token");
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }
  // Future<List<Categorie>> fetchCategories() async {
  //   final response = await http.get(
  //     Uri.parse('http://192.168.43.117:9090/api/categories'),
  //     headers: {'Content-Type': 'application/json'},
  //   );
  //   if (response.statusCode == 200 && response.body != null) {
  //     Iterable jsonList = json.decode(response.body);
  //     return jsonList.map((model) => Categorie.fromJson(model)).toList();
  //   } else {
  //     throw Exception('Failed to load categories');
  //   }
  // }
  Future<void> fetchCategories() async {
    try {
      final token = await storage.read(key: "jwt_token");
      final url = 'http://192.168.43.117:9090/api/categories';
      final encodedUrl = Uri.encodeFull(url);
      final response = await http.get(
        Uri.parse(encodedUrl),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8', // set the content type to json and encoding to utf-8
          'Authorization': 'Bearer $token',
          'Accept': 'application/json;charset=UTF-8', // set the accept header to json and encoding to utf-8
        },
      );
      if (response.statusCode == 200 && response.body != null) {
        final Iterable jsonList = json.decode(utf8.decode(response.bodyBytes)); // decode the response using utf-8 encoding
        _categories.clear();
        _categories.addAll(jsonList.map((model) => Categorie.fromJson(model)));
      } else {
        throw Exception('Failed to load categories');
      }
    } catch (e) {
      print('Failed to fetch categories: $e');
    }
  }
  Future<void> updateCategorie(int idCategorie, Categorie updatedCategorie) async {
    final response = await http.put(Uri.parse('http://192.168.43.117:9090/api/categories/$idCategorie'),
        headers: {'Content-Type': 'application/json',
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT, DELETE, HEAD",},
        body: json.encode(updatedCategorie.toJson()));
    if (response.statusCode == 200) {
      final index = _categories.indexWhere((categorie) => categorie.idCategorie == idCategorie);
      if (index != -1) {
        _categories[index] = updatedCategorie;
      }
    } else {
      // Handle error
      print('Failed to update categorie: ${response.statusCode}');
    }
  }

  Future<Categorie> getOneCategorie(int idCategorie) async {
    final response = await http.get(Uri.parse('http://192.168.43.117:9090/api/categories/$idCategorie'));
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      final Categorie categorie = Categorie.fromJson(responseData);
      return categorie;
    } else {
      // Handle error
      print('Failed to get categorie: ${response.statusCode}');
      throw Exception('Failed to get categorie');
    }
  }

  Future<void> deleteCategorie(int idCategorie) async {
    final response = await http.delete(Uri.parse('http://192.168.43.117:9090/api/categories/$idCategorie'));
    if (response.statusCode == 200) {
      _categories.removeWhere((categorie) => categorie.idCategorie == idCategorie);
    } else {
      // Handle error
      print('Failed to delete categorie: ${response.statusCode}');
    }
  }
  Future<void> addCategorie(Categorie categorie) async {
    final response = await http.post(Uri.parse('http://192.168.43.117:9090/api/categories'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(categorie.toJson()));
    if (response.statusCode == 201) {
      final responseData = json.decode(response.body);
      final addedCategorie = Categorie.fromJson(responseData);
      _categories.add(addedCategorie);
    } else {
      // Handle error
      print('Failed to add categorie: ${response.statusCode}');
    }
  }
}