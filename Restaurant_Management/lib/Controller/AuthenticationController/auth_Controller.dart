import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gestion_restaurantion/Models/User.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

final storage = FlutterSecureStorage();

class AuthController extends GetxController {
  var isLoggedIn = false.obs;

  Future<bool> login(String email, String password) async {
    try {
      final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/auth/login'),
        headers: {'Content-Type': 'application/json'},
        body: json.encode({'email': email, 'password': password}),
      );
      if (response.statusCode == 200) {
        final token = response.body;
        await storage.write(key: 'jwt_token', value: token);
        isLoggedIn.value = true;
        return true;
      } else {
        // Handle invalid credentials
        Get.snackbar(
          'Erreur',
          'email ou password Invalide',
          backgroundColor: Colors.red,
          colorText: Colors.white,
          snackPosition: SnackPosition.BOTTOM,
        );
        isLoggedIn.value = false;
        return false;
      }
    } catch (e) {
      // Handle network errors
      Get.snackbar(
        'Error',
        'Network error',
        backgroundColor: Colors.red,
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
      );
      isLoggedIn.value = false;
      return false;
    }
  }

  Future<bool> signup(String name, String email, String password) async {
    try {
      final response = await http.post(
        Uri.parse('http://192.168.43.117:9090/api/auth/signup'),
        headers: {'Content-Type': 'application/json'},
        body: json.encode({'name': name, 'email': email, 'password': password}),
      );

      if (response.statusCode == 201) {
        final token = response.body;
        await storage.write(key: 'jwt_token', value: token);
        isLoggedIn.value = true;
        return true;
      } else {
        // Handle registration errors
        Get.snackbar(
          'Error',
          'Registration failed',
          backgroundColor: Colors.red,
          colorText: Colors.white,
          snackPosition: SnackPosition.BOTTOM,
        );
        isLoggedIn.value = false;
        return false;
      }
    } catch (e) {
      // Handle network errors
      Get.snackbar(
        'Error',
        'Network error',
        backgroundColor: Colors.red,
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
      );
      isLoggedIn.value = false;
      return false;
    }
  }

  Future<void> signOut() async {
    isLoggedIn.value = false;
    await storage.delete(key: 'jwt_token');
  }


}