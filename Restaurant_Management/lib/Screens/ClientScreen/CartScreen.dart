import 'dart:async';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/CategorieController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/ClientController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/CommandeController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/LigneCommadeController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/TableRestaurantController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/UserController.dart';
import 'package:gestion_restaurantion/Models/TableRestaurant.dart';
import 'package:gestion_restaurantion/Models/ligneCommande.dart';
import 'package:gestion_restaurantion/Models/ligneCommande.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../Models/Commande.dart';
import '../../../Models/User.dart';
import '../../../Models/ligneCommande.dart';
import 'package:gestion_restaurantion/Screens/Utils/AppColor.dart';

import 'MenuListScreen.dart';

import 'NotificationManger.dart';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class ShoppingCartScreen extends StatefulWidget {
  @override
  State<ShoppingCartScreen> createState() => _ShoppingCartScreenState();
}

class _ShoppingCartScreenState extends State<ShoppingCartScreen> {

  @override
  Widget build(BuildContext context) {
    final cartController = Get.find<ShoppingCartController>();
    final clientController = Get.find<ClientController>();
    final userController = Get.find<UserController>();
    final tableController = Get.find<TableRestaurantController>();
    final commandeController = Get.find<CommandeController>();
    final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
    int? _tableNumber;

    bool isScanningEnabled = false  ;

    final TextEditingController _tableNumberController = TextEditingController();

    String _specialInstructions = "";

    Future<String?> _getUserEmail() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('email');
    }

//     late QRViewController _qrViewController;
// // Define the _onQRViewCreated method
//     void _onQRViewCreated(QRViewController controller) {
//       setState(() {
//         _qrViewController = controller;
//         _qrViewController.scannedDataStream.listen((scanData) {
//           setState(() {
//             int? scannedTableNumber;
//             if (scanData.code != null) {
//               scannedTableNumber = int.tryParse(scanData.code!);
//               _tableNumberController.text = scannedTableNumber.toString();
//             }
//             _tableNumber = scannedTableNumber;
//           });
//         });
//       });
//     }
//
//     // Override the dispose method to stop the camera when the widget is disposed
//     @override
//     void dispose() {
//       _qrViewController.dispose();
//       super.dispose();
//     }
//     void _startScan() {
//       setState(() {
//         isScanningEnabled = true; // Enable scanning
//       });
//       if (_qrViewController != null) {
//         _qrViewController!.resumeCamera();
//       }
//     }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primary,
        leading: Image.network(
            "https://cdn-icons-png.flaticon.com/128/3514/3514242.png"),
        title: RichText(
          text: TextSpan(
            style: TextStyle(fontSize: 20.0),
            children: [
              TextSpan(
                text: 'Mon ',
                style: TextStyle(color: AppColor.secondary),
              ),
              TextSpan(
                text: 'Panier',
                style: TextStyle(color: AppColor.secondary),
              ),
            ],
          ),
        ),
        elevation: 2.0,
      ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white10, Color(0xFFD6D6D6)],
            ),
          ),
          child: Obx(() {
            if (cartController.cartItems.isEmpty) {
              return Center(
                child: Text(
                  'Le panier est vide',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.grey[700]),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: cartController.cartItems.length,
                itemBuilder: (context, index) {
                  final cartItem = cartController.cartItems[index];
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    elevation: 4,
                    margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: ListTile(
                      contentPadding: EdgeInsets.all(16),
                      leading: Container(
                        width: 60,
                        height: 100,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.memory(
                            cartItem.article.imageBytes,
                            fit: BoxFit.cover
                          ),
                        ),
                      ),
                      title: Text(
                        cartItem.article.libelle.toUpperCase(),
                        style: TextStyle(
                          fontSize: 20,
                          color: AppColor.primary,
                          fontFamily: 'Varela',
                          fontStyle:
                          FontStyle
                              .italic,
                          // add italic style
                          fontWeight:
                          FontWeight
                              .bold,
                          // add bold font weight
                          letterSpacing:
                          1.2,
                          // add spacing between letters
                          wordSpacing:
                          2.0, // add spacing between words
                        ),),
                      subtitle: Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.monetization_on, color: Colors.green, size: 18),
                            SizedBox(width:1),
                            Text(
                              '${(cartItem.article.prix * cartItem.quantite).toStringAsFixed(1)}',
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            onPressed: () => cartController.incrementQuantity(cartItem),
                            icon: Icon(Icons.add),
                            color: Colors.blue,
                          ),
                          Text(
                            '${cartItem.quantite.toInt()}',
                            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          IconButton(
                            onPressed: () => cartController.decrementQuantity(cartItem),
                            icon: Icon(Icons.remove),
                            color: Colors.blue,
                          ),
                          IconButton(
                            onPressed: () => cartController.removeItem(cartItem),
                            icon: Icon(Icons.delete),
                            color: Colors.red,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            }
          }),
        ),



        bottomNavigationBar: Container(
        height: 120.0,
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.vertical(top: Radius.circular(16.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 5,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'TOTALE :',//
                  style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold,color: Colors.grey[700],letterSpacing: 1.2),
                ),
                Text(
                  ' ${cartController.getTotalCost()}',//${cartController.getTotalCost()} Dinar
                  style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold,color: AppColor.primary,letterSpacing: 1.1),
                ),
              ],
            )),
            SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: () {
                    cartController.clearCart();
                    Navigator.pop(context);
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.redAccent, // Change the button color
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0), // Increase the border radius
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0), // Adjust the padding
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.delete, color: AppColor.secondary), // Add an icon
                        SizedBox(width: 3.0), // Add some spacing between the icon and text
                        Text(
                          'Vider le Panier', // Keep the text as is
                          style: TextStyle(fontSize: 11.0,color: AppColor.secondary),
                        ),
                      ],
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    if(cartController.cartItems.isEmpty){
                      AwesomeDialog(
                        context: context,
                        dialogType: DialogType.error,
                        title: 'Erreur',
                        desc: 'Votre panier est vide.',
                        btnOkText: 'OK',
                        btnOkColor: AppColor.primary,
                        btnOkOnPress: () {
                          Navigator.pop(context); // Navigate back to the previous page
                        },
                        dismissOnTouchOutside: false,
                        dismissOnBackKeyPress: false,
                      )..show();
                    }
                    else {
                      // Show an AwesomeDialog to ask if the order is takeaway or for in-restaurant dining
                      AwesomeDialog(
                        context: context,
                        dialogType: DialogType.question,
                        title: 'Choisissez le type de commande',
                        desc: 'Veuillez choisir le type de commande :',
                        btnOkText: 'A emporter',
                        btnOkColor: AppColor.primary,
                        btnCancelText: 'Sur place',
                        btnCancelColor: AppColor.primary,
                        //todo : partie A emporter
                        btnOkOnPress: () {
                          // Handle "A emporter" button click
                          AwesomeDialog(
                              context: context,
                              dialogType: DialogType.info,
                              animType: AnimType.BOTTOMSLIDE,
                              title: 'A emporter',
                              desc: 'Veuillez renseigner les instructions spéciales (facultatif):',
                              body: Column(
                                children: [
                                  TextFormField(
                                    decoration: InputDecoration(
                                      hintText: 'Instructions spéciales (facultatif)',
                                    ),
                                    onChanged: (value) {
                                      _specialInstructions = value;
                                    },
                                  ),
                                ],
                              ),
                              btnOkText: 'Valider',
                              btnOkColor: AppColor.primary,
                              btnCancelText: 'Annuler',
                              btnCancelColor: AppColor.primary,
                              btnCancelOnPress: () {},
                              btnOkOnPress: () async {
                                List<ligneCommande>
                                ligneCommandesList = [];
                                for (ligneCommande item
                                in cartController.cartItems) {
                                  ligneCommande? itemx =
                                  await cartController
                                      .saveCartItemToDatabase(
                                      item);
                                  // print(item.idLigneCommande);
                                  ligneCommandesList.add(itemx!);
                                }
                                //todo fix the problem : lezemni njib el table bel number mte3ha
                                final table = tableController
                                    .getTableByName(0);
                                final tablex = await table;
                                print("table:");
                                print(tablex?.idTable);

                                print(
                                    "instruction special :  $_specialInstructions");

                                //todo : njib lclient ama lezem 9bal njib  the USER
                                print("email : ");
                                // print('Email: $userEmail');
                                String? userEmail =
                                await _getUserEmail();
                                final user = userController
                                    .getUserByEmail(userEmail!);
                                final userx = await user;
                                print(userx.email);

                                print("client : ");
                                final client = clientController
                                    .getClientByUser(
                                    userx.idUser);
                                final clientx = await client;
                                print(clientx.firstname);
                                //todo : nasna3 commnande
                                final Commande commande =
                                Commande(
                                  idCommande: 1,
                                  date: DateTime.now(),
                                  etat: Etat.enCours,
                                  description:
                                  _specialInstructions,
                                  ligneCommandes:
                                  ligneCommandesList,
                                  client: clientx,
                                  tableRestaurant: tablex!,
                                );
                                final lacommande =
                                await commandeController
                                    .saveCommande(commande);
                                AwesomeDialog(
                                  context: context,
                                  dialogType: DialogType.SUCCES,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Commande confirmée',
                                  desc: 'Votre commande a été enregistrée avec succès!',
                                  btnOkText: 'Fermer',
                                  btnOkColor: AppColor.primary,
                                  btnOkOnPress: () {
                                    cartController.clearCart();
                                    Navigator.pop(context);
                                  },
                                )
                                  ..show();
                              }
                          )
                            ..show();
                        },
                        //Todo : partie SurPlace
                        btnCancelOnPress: () {
                          AwesomeDialog(
                            context: context,
                            dialogType: DialogType.info,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'Sur place',
                            desc: 'Veuillez renseigner le numéro de table et des instructions spéciales:',
                            body: Column(
                              children: [
                                //todo PARTIE QR
                                // Container(
                                //   height: 250, // Set the desired height for the QR code scanner widget
                                //   child: Visibility(
                                //     visible: isScanningEnabled ,
                                //     child: QRView(
                                //       key: qrKey,
                                //       onQRViewCreated: _onQRViewCreated, // Assign the _onQRViewCreated method to the onQRViewCreated callback
                                //       overlay: QrScannerOverlayShape(
                                //         borderColor: Theme.of(context).accentColor,
                                //         borderRadius: 10,
                                //         borderLength: 30,
                                //         borderWidth: 10,
                                //         cutOutSize: MediaQuery.of(context).size.width * 0.8,
                                //       ),
                                //     ),
                                //   ),
                                // ),
                                TextFormField(
                                  controller: _tableNumberController,
                                  decoration: InputDecoration(
                                    hintText: 'Numéro de table',
                                  ),
                                  onChanged: (value) {
                                    _tableNumber = int.tryParse(value) ?? 0;
                                  },
                                ),
                                // ElevatedButton(
                                //   onPressed: _startScan, // Call the _startScan() method when the button is pressed
                                //   child: Text('Scan QR code'),
                                // ),
                                SizedBox(height: 10.0),
                                TextFormField(
                                  decoration: InputDecoration(
                                    hintText: 'Instructions spéciales (facultatif)',
                                  ),
                                  onChanged: (value) {
                                    _specialInstructions = value;
                                  },
                                ),
                              ],
                            ),
                            btnOkText: 'Valider',
                            btnOkColor: AppColor.primary,
                            btnCancelText: 'Annuler',
                            btnCancelColor: AppColor.primary,
                            btnOkOnPress: () async {
                              if (_tableNumber != null &&
                                  _tableNumber! <=
                                      tableController.tableRestaurants.length &&
                                  _tableNumber is int &&
                                  _tableNumber! > 0) {
                                List<ligneCommande> ligneCommandesList = [];
                                for (ligneCommande item in cartController
                                    .cartItems) {
                                  ligneCommande? itemx =
                                  await cartController.saveCartItemToDatabase(
                                      item);
                                  ligneCommandesList.add(itemx!);
                                }

                                final table = tableController.getTableByName(
                                    _tableNumber!);
                                final tablex = await table;

                                String? userEmail = await _getUserEmail();
                                final user = userController.getUserByEmail(
                                    userEmail!);
                                final userx = await user;
                                final client = clientController.getClientByUser(
                                    userx.idUser);
                                final clientx = await client;

                                final Commande laCommande = Commande(
                                  idCommande: 1,
                                  date: DateTime.now(),
                                  etat: Etat.enCours,
                                  description: _specialInstructions,
                                  ligneCommandes: ligneCommandesList,
                                  client: clientx,
                                  tableRestaurant: tablex!,
                                );
                                await commandeController.saveCommande(
                                    laCommande);
                                AwesomeDialog(
                                  context: context,
                                  dialogType: DialogType.SUCCES,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Commande confirmée',
                                  desc: 'Votre commande a été enregistrée avec succès!',
                                  btnOkText: 'Fermer',
                                  btnOkColor: AppColor.primary,
                                  btnOkOnPress: () {
                                    cartController.clearCart();
                                    Navigator.pop(context);
                                  },
                                )
                                  ..show();
                              } else {
                                AwesomeDialog(
                                  context: context,
                                  dialogType: DialogType.ERROR,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Numéro de table invalide',
                                  desc: 'Veuillez entrer un numéro de table valide',
                                  btnOkText: 'Fermer',
                                  btnOkColor: AppColor.primary,
                                  btnOkOnPress: () {
                                    Navigator.pop(context);
                                  },
                                )
                                  ..show();
                              }
                            },
                            btnCancelOnPress: () {},
                          )
                            ..show();
                        },
                      )
                        ..show();
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: AppColor.primary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.shopping_cart, color :AppColor.secondary),
                        SizedBox(width: 3.0),
                        Text(
                          'Passer commande',
                          style: TextStyle(fontSize: 11.0,color :AppColor.secondary),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
        ),
    ]
      ),
      )
    );
  }
}
