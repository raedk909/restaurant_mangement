import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:gestion_restaurantion/Controller/AuthenticationController/auth_Controller.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/ArticleController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/CategorieController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/LigneCommadeController.dart';

import 'package:gestion_restaurantion/Models/Article.dart';
import 'package:gestion_restaurantion/Models/Catgorie.dart';
import 'package:gestion_restaurantion/Models/sous_Categorie.dart';
import 'package:gestion_restaurantion/Screens/AuthScreen/WelcomePage.dart';

import 'package:get/get.dart';

import 'package:flutter_sequence_animation/flutter_sequence_animation.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/RatingController.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:gestion_restaurantion/Screens/Utils/AppColor.dart';

import 'CartScreen.dart';
class MenuListScreen extends StatefulWidget {
  @override
  _MenuListScreenState createState() => _MenuListScreenState();
}

class _MenuListScreenState extends State<MenuListScreen>
    with SingleTickerProviderStateMixin {

  final CategorieController categorieController =
  Get.put(CategorieController());

  final TextEditingController searchController = TextEditingController();

  final ShoppingCartController shoppingCartController =
  Get.put(ShoppingCartController());
  final ArticleController articleController = Get.put(ArticleController());
  final AuthController authController = Get.put(AuthController());

  final RatingController ratingController = Get.put(RatingController());

  List<Categorie> filteredCategories = [];
  Categorie? selectedCategorie;

  List<Articles> filteredArticles = [];
  List<Articles> filteredArticles1= [];

  void _signOut() async {
   await AwesomeDialog(
      context: context,
      dialogType: DialogType.question,
      animType: AnimType.scale,
      title: 'Déconnexion',
      desc: 'Êtes-vous sûr de vouloir vous déconnecter?',
      btnOkText: 'Oui',
      btnOkColor: AppColor.primary,
      btnCancelText: 'Non',
      btnCancelColor: AppColor.primary,
      btnOkOnPress: () async {
        await Get.find<AuthController>().signOut();
        // Redirect the user to the login screen or wherever you want to redirect them after sign out
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => WelcomePage()));
      },
      btnCancelOnPress: () {},
    ).show();
  }


//todo : Partie Animation
  late AnimationController _animationController;
  late Animation<double> _scaleAnimation;
  late Animation<double> _rotationAnimation;
  late Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();
    filteredCategories = categorieController.categories;

    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    _scaleAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(0, 0.5, curve: Curves.easeIn),
      ),
    );

    _rotationAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.5, 1.0, curve: Curves.easeInOut),
      ),
    );

    _rotationAnimation.addListener(() {
      setState(() {});
    });

    _opacityAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.5, 1.0, curve: Curves.easeInOut),
      ),
    );
    Future.delayed(Duration(seconds: 2), () {
      _animationController.reverse();
    });

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  bool _isSearchClicked = false;
  void _onSearchClicked() {
    setState(() {
      _isSearchClicked = true;
    });
  }
  void _onSearchCancelled() {
    setState(() {
      _isSearchClicked = false;
      searchController.clear();
      filteredArticles1.clear();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //todo ma8ir app bar
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: Center(
              child: AnimatedBuilder(
                animation: _animationController,
                builder: (context, child) {
                  return Transform.scale(
                    scale: _scaleAnimation.value,
                    child: Opacity(
                      opacity: _opacityAnimation.value,
                      child: Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.identity()..setEntry(3, 2, 0.001),
                        // ..rotateY(_rotationAnimation.value * pi),
                        child: Image.network(
                          'https://tse3.mm.bing.net/th?id=OIP.3Hoax8b9oiPwButKCp8WZgAAAA&pid=Api&P=0',
                          fit: BoxFit.contain,
                          width: double.infinity,
                          height: double.infinity,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
      // Display the main content of the screen in the foreground
            Visibility(
              visible: _animationController.isDismissed,
              child: Stack(
                children :[ Container(
                  decoration: BoxDecoration(
                   // color: AppColor.secondary, // Replace with desired background color
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 90,),
                      Image.asset(
                        'assets/images/menulogo.png',
                        fit: BoxFit.cover,
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(height: 60),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: Offset(0, 3),
                              ),
                            ],
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 5,
                                  offset: Offset(0, 3),
                                ),
                              ],
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                    child: TextField(
                                      controller: searchController,
                                      decoration: InputDecoration(
                                        hintText: "Rechercher",
                                        border: InputBorder.none,
                                      ),
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.grey[800],
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          filteredArticles1 = categorieController.categories
                                              .map((categorie) => categorie.articles.where((article) =>
                                              article.libelle.toLowerCase().contains(value.toLowerCase()))
                                              .toList())
                                              .expand((x) => x).toList();
                                        });
                                      },
                                    ),
                                  ),
                                ),
                                Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(10),
                                    onTap: _onSearchClicked,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        Icons.search,
                                        size: 28,
                                        color: Colors.grey[800],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 8,),
                      if (_isSearchClicked)
                        Expanded(
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: _onSearchCancelled,
                                icon: Icon(Icons.arrow_back),
                              ),
                              Expanded(
                                child: ListView.builder(
                                  itemCount:  filteredArticles1.length,
                                  itemBuilder: (BuildContext context, int index) {
                                    final article =  filteredArticles1[index];
                                    return GestureDetector(
                                      onTap: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {



                                            //todo hethi el details mte3 les articles eli fel search
                                            return AlertDialog(
                                              //todo hethi el details mte3 les articles eli ta7t les categorie
                                              backgroundColor: Colors.white,
                                              content: Container(
                                                width: double.maxFinite,
                                                child: Center(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    // Aligns the text to the top of the card
                                                    children: [
                                                      Row(
                                                        children: [
                                                          SizedBox(
                                                            width: 30,
                                                            child: IconButton(
                                                              icon: Icon(Icons.arrow_back),
                                                              onPressed: () => Navigator.of(context).pop(),
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                              article.libelle
                                                                  .toUpperCase(),
                                                              textAlign: TextAlign.center,
                                                              // Makes the title all caps for emphasis
                                                              style: TextStyle(
                                                                fontSize: 30,
                                                                color: AppColor.primary,
                                                                fontFamily: 'Varela',
                                                                fontStyle:
                                                                FontStyle
                                                                    .italic,
                                                                // add italic style
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold,
                                                                // add bold font weight
                                                                letterSpacing:
                                                                1.2,
                                                                // add spacing between letters
                                                                wordSpacing:
                                                                2.0, // add spacing between words
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Center(
                                                        child: Container(
                                                          width: MediaQuery.of(context).size.width * 0.6,
                                                          // Adjust the width of the container to fit the image for most phones
                                                          height: MediaQuery.of(context).size.height * 0.3,
                                                          // Adjust the height of the container to fit the image for most phones
                                                          child: ClipRRect(
                                                            // Clips the image to a rounded rectangle for a more polished look
                                                            borderRadius:
                                                            BorderRadius.circular(6),
                                                            child: Image.memory(
                                                              article.imageBytes,
                                                              fit: BoxFit.cover,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: 8),
                                                      // Adds some space between the image and the text
                                                      Row(
                                                        children: [
                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                              CrossAxisAlignment.start,
                                                              children: [
                                                                SizedBox(height: 14),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: [

                                                                    SizedBox(height: 14),
                                                                    Row(
                                                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                      children: [
                                                                        Column(
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          children: [
                                                                            Text(
                                                                              "PRIX",
                                                                              style: TextStyle(
                                                                                fontSize: 18,
                                                                                color: Colors.grey[700],
                                                                                fontWeight: FontWeight.bold,
                                                                                letterSpacing: 1.2,
                                                                              ),
                                                                            ),
                                                                            SizedBox(height: 4),
                                                                            Text(
                                                                              '${article.prix} Dinar',
                                                                              style: TextStyle(
                                                                                fontSize: 20,
                                                                                color: AppColor.primary,
                                                                                fontWeight: FontWeight.bold,
                                                                                letterSpacing: 1.1,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        Column(
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          children: [
                                                                            Text(
                                                                              "DURÉE",
                                                                              style: TextStyle(
                                                                                fontSize: 18,
                                                                                color: Colors.grey[700],
                                                                                fontWeight: FontWeight.bold,
                                                                                letterSpacing: 1.2,
                                                                              ),
                                                                            ),
                                                                            SizedBox(height: 4),
                                                                            Text(
                                                                              '${article.duree} Min',
                                                                              style: TextStyle(
                                                                                fontSize: 20,
                                                                                color: AppColor.primary,
                                                                                fontWeight: FontWeight.bold,
                                                                                letterSpacing: 1.1,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    SizedBox(height: 12),
                                                                    Text(
                                                                      "DESCRIPTION",
                                                                      style: TextStyle(
                                                                        fontSize: 18,
                                                                        color: Colors.grey[700],
                                                                        fontWeight: FontWeight.bold,
                                                                        letterSpacing: 1.2,
                                                                      ),
                                                                    ),
                                                                    SizedBox(height: 4),
                                                                    Text(
                                                                      article.description,
                                                                      textAlign: TextAlign.center,
                                                                      style: TextStyle(
                                                                        fontSize: 16,
                                                                        color: AppColor.primary,
                                                                        letterSpacing: 1.1,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ), SizedBox(height: 8),
                                                                Row(
                                                                  children: [
                                                                    Expanded(
                                                                      child: Divider(
                                                                        color: Colors.grey,
                                                                        height: 50,
                                                                        thickness: 1,
                                                                      ),
                                                                    ),
                                                                    SizedBox(width: 16),
                                                                    InkWell(
                                                                      child: Icon(
                                                                          Icons.favorite,
                                                                          color: Colors.red,
                                                                          size: 50
                                                                      ),
                                                                    ),
                                                                    SizedBox(width: 16),
                                                                    Expanded(
                                                                      child: Divider(
                                                                        color: Colors.grey,
                                                                        height: 50,
                                                                        thickness: 1,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                SizedBox(height: 10),
                                                                Row(
                                                                    mainAxisAlignment: MainAxisAlignment.center, // Updated
                                                                    children: [
                                                                      ElevatedButton.icon(
                                                                        onPressed: () {
                                                                          //addToCart(article);
                                                                          final cartController = Get.find<ShoppingCartController>();
                                                                          cartController.addItem(article, 1);
                                                                          ScaffoldMessenger.of(context).showSnackBar(
                                                                            SnackBar(
                                                                              content: Text('${article.libelle} ajouté au panier'),
                                                                              duration: Duration(seconds: 1),
                                                                            ),
                                                                          );
                                                                        },
                                                                        icon: Icon(Icons.shopping_cart,color: AppColor.secondary,),
                                                                        label: Text(
                                                                          'Ajouter au panier',
                                                                          style: TextStyle(
                                                                              fontSize: 16.0,
                                                                              fontWeight: FontWeight.bold,
                                                                              color: AppColor.secondary
                                                                          ),
                                                                        ),
                                                                        style: ElevatedButton.styleFrom(
                                                                          primary: AppColor.primary,
                                                                          onPrimary: AppColor.primary,
                                                                          shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(10.0),
                                                                          ),
                                                                          elevation: 2.0,
                                                                          padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
                                                                        ),
                                                                      ),
                                                                    ]
                                                                ),

                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      },
                                      //todo hethi el card mte3 les articles ki na3mel search
                                      child: Card(
                                        elevation: 4,
                                        // Adds a shadow to the card for depth
                                        child: Center(
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            // Aligns the text to the top of the card
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      article.libelle
                                                          .toUpperCase(),
                                                      textAlign: TextAlign.center,
                                                      // Makes the title all caps for emphasis
                                                      style: TextStyle(
                                                        fontSize: 30,
                                                        color: AppColor.primary,
                                                        fontFamily: 'Varela',
                                                        fontStyle:
                                                        FontStyle
                                                            .italic,
                                                        // add italic style
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        // add bold font weight
                                                        letterSpacing:
                                                        1.2,
                                                        // add spacing between letters
                                                        wordSpacing:
                                                        2.0, // add spacing between words
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Center(
                                                child: Container(
                                                  width: MediaQuery.of(context).size.width * 0.6,
                                                  // Adjust the width of the container to fit the image for most phones
                                                  height: MediaQuery.of(context).size.height * 0.3,
                                                  // Adjust the height of the container to fit the image for most phones
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(6),
                                                    child: Image.memory(
                                                      article.imageBytes,
                                                      fit: BoxFit.contain,
                                                      // Use BoxFit.contain to fit the entire image within the container without being cropped or distorted
                                                    ),
                                                  ),
                                                ),
                                              ),

                                              SizedBox(width: 16),
                                              // Adds some space between the image and the text
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: [
                                                        Center(
                                                          child: RatingBar.builder(
                                                            initialRating: articleController.calculateAverageRating(article),
                                                            minRating: 1,
                                                            direction: Axis.horizontal,
                                                            allowHalfRating: true,
                                                            itemCount: 5,
                                                            itemSize: 30,
                                                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                                            itemBuilder: (context, _) => Icon(
                                                              Icons.star,
                                                              color: Colors.amber,
                                                              size: 35,
                                                            ),
                                                            onRatingUpdate: (rating) async {
                                                              print(rating);
                                                              final Rating r1 = Rating(value: rating);
                                                              final R1 = await ratingController.addRating(r1);
                                                              print(R1?.id);
                                                              article.ratings.add(R1!);
                                                              await articleController.updateArticle(article);
                                                              showDialog(
                                                                context: context,
                                                                builder: (BuildContext context) {
                                                                  // TODO : hethi el remerciement ki na3mel rating le stars
                                                                  return AlertDialog(
                                                                    backgroundColor: AppColor.primary,
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(16.0),
                                                                    ),
                                                                    title: Text(
                                                                      "Merci beaucoup pour votre avis sur notre article de menu ${article.libelle}.",
                                                                      style: TextStyle(
                                                                        color : AppColor.secondary,
                                                                        fontSize: 20.0,
                                                                        fontWeight: FontWeight.bold,
                                                                        fontStyle: FontStyle.italic,
                                                                      ),
                                                                    ),
                                                                    content: Padding(
                                                                      padding: EdgeInsets.symmetric(vertical: 8.0),
                                                                      child: Text(
                                                                        "Nous sommes ravis que vous ayez apprécié votre choix et nous espérons vous revoir bientôt chez nous.",
                                                                        style: TextStyle(
                                                                          color : AppColor.secondary,
                                                                          fontSize: 16.0,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    actions: [
                                                                      ElevatedButton(
                                                                        style: ElevatedButton.styleFrom(
                                                                          primary:   AppColor.primary,
                                                                          shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(8.0),
                                                                          ),
                                                                        ),
                                                                        onPressed: () {
                                                                          Navigator.pop(context);
                                                                        },
                                                                        child: Text(
                                                                          'OK',
                                                                          style: TextStyle(
                                                                            color:   AppColor.secondary,
                                                                            fontSize: 16.0,
                                                                            fontWeight: FontWeight.bold,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  );
                                                                },
                                                              );
                                                            },
                                                          ),
                                                        ),
                                                        SizedBox(height: 24),
                                                        Column(
                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                          children: [

                                                            SizedBox(height: 24),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                              children: [
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: [
                                                                    Text(
                                                                      "PRIX",
                                                                      style: TextStyle(
                                                                        fontSize: 18,
                                                                        color: Colors.grey[700],
                                                                        fontWeight: FontWeight.bold,
                                                                        letterSpacing: 1.2,
                                                                      ),
                                                                    ),
                                                                    SizedBox(height: 8),
                                                                    Text(
                                                                      '${article.prix} €',
                                                                      style: TextStyle(
                                                                        fontSize: 20,
                                                                        color: AppColor.primary,
                                                                        fontWeight: FontWeight.bold,
                                                                        letterSpacing: 1.1,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: [
                                                                    Text(
                                                                      "DURÉE",
                                                                      style: TextStyle(
                                                                        fontSize: 18,
                                                                        color: Colors.grey[700],
                                                                        fontWeight: FontWeight.bold,
                                                                        letterSpacing: 1.2,
                                                                      ),
                                                                    ),
                                                                    SizedBox(height: 8),
                                                                    Text(
                                                                      '${article.duree} Min',
                                                                      style: TextStyle(
                                                                        fontSize: 20,
                                                                        color: AppColor.primary,
                                                                        fontWeight: FontWeight.bold,
                                                                        letterSpacing: 1.1,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            SizedBox(height: 24),
                                                            Text(
                                                              "DESCRIPTION",
                                                              style: TextStyle(
                                                                fontSize: 18,
                                                                color: Colors.grey[700],
                                                                fontWeight: FontWeight.bold,
                                                                letterSpacing: 1.2,
                                                              ),
                                                            ),
                                                            SizedBox(height: 8),
                                                            Text(
                                                              article.description,
                                                              textAlign: TextAlign.center,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: AppColor.primary,
                                                                letterSpacing: 1.1,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(height: 8),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                              child: Divider(
                                                                color: Colors.grey,
                                                                height: 50,
                                                                thickness: 1,
                                                              ),
                                                            ),
                                                            SizedBox(width: 16),
                                                            InkWell(
                                                              child: Icon(
                                                                  Icons.favorite,
                                                                  color: Colors.red,
                                                                  size: 50
                                                              ),
                                                            ),
                                                            SizedBox(width: 16),
                                                            Expanded(
                                                              child: Divider(
                                                                color: Colors.grey,
                                                                height: 50,
                                                                thickness: 1,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(height: 16),
                                                        Row(
                                                            mainAxisAlignment: MainAxisAlignment.center, // Updated
                                                            children: [
                                                              ElevatedButton.icon(
                                                                onPressed: () {
                                                                  //addToCart(article);
                                                                  final cartController = Get.find<ShoppingCartController>();
                                                                  cartController.addItem(article, 1);
                                                                  ScaffoldMessenger.of(context).showSnackBar(
                                                                    SnackBar(
                                                                      content: Text('${article.libelle} ajouté au panier'),
                                                                      duration: Duration(seconds: 1),
                                                                    ),
                                                                  );
                                                                },
                                                                icon: Icon(Icons.shopping_cart,color: AppColor.secondary,),
                                                                label: Text(
                                                                  'Ajouter au panier',
                                                                  style: TextStyle(
                                                                      fontSize: 16.0,
                                                                      fontWeight: FontWeight.bold,
                                                                      color: AppColor.secondary
                                                                  ),
                                                                ),
                                                                style: ElevatedButton.styleFrom(
                                                                  primary: AppColor.primary,
                                                                  onPrimary: AppColor.primary,
                                                                  shape: RoundedRectangleBorder(
                                                                    borderRadius: BorderRadius.circular(10.0),
                                                                  ),
                                                                  elevation: 2.0,
                                                                  padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
                                                                ),
                                                              ),
                                                            ]
                                                        ),

                                                        SizedBox(height: 18),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),





                      SizedBox(height: 30),
                      if (!_isSearchClicked)
                        Expanded(
                          child: Obx(() {
                            final order = [
                              "Entree",
                              "Plat Chauds",
                              "Desserts",
                              "Boissons"
                            ];
                            categorieController.categories.sort((a, b) =>
                                order.indexOf(a.nom).compareTo(order.indexOf(b.nom)));
                            if (categorieController.categories.isEmpty) {
                              return Center(child: CircularProgressIndicator());
                            } else {
                              return ListView.builder(
                                itemCount: filteredCategories.length,
                                itemBuilder: (BuildContext context, int index) {
                                  final categorie = filteredCategories[index];
                                  return Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                            color: AppColor.secondary,
                                          ),
                                        ),
                                      ),
                                      child: ExpansionTile(
                                        maintainState: true,
                                        leading: Image.memory(
                                          categorie.imageBytes,
                                          fit: BoxFit.cover,
                                        ),
                                        title: Text(
                                          categorie.nom,
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        onExpansionChanged: (expanded) {
                                          if (expanded &&
                                              categorie != selectedCategorie) {
                                            // If the current tile is being expanded and is different from the selected category
                                            if (selectedCategorie != null) {
                                              // If there is a currently expanded category, collapse it first
                                              setState(() {
                                                selectedCategorie = null;
                                                filteredArticles = [];
                                              });
                                            }
                                            setState(() {
                                              selectedCategorie = categorie;
                                              filteredArticles =
                                                  selectedCategorie!.articles;
                                            });
                                          } else if (!expanded &&
                                              categorie == selectedCategorie) {
                                            // If the current tile is being collapsed and is the same as the selected category
                                            setState(() {
                                              selectedCategorie = null;
                                              filteredArticles = [];
                                            });
                                          }
                                        },
                                        children: [
                                          Column(
                                            children: filteredArticles.map((article) {
                                              return GestureDetector(
                                                onTap: () {
                                                  showDialog(
                                                    context: context,
                                                    builder: (BuildContext context) {
                                                      return AlertDialog(




                                                        //todo hethi el details mte3 les articles eli ta7t les categorie
                                                        backgroundColor: Colors.white,
                                                        content: Container(
                                                          width: double.maxFinite,
                                                          child: Center(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                              CrossAxisAlignment.start,
                                                              // Aligns the text to the top of the card
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    SizedBox(
                                                                      width: 30,
                                                                      child: IconButton(
                                                                        icon: Icon(Icons.arrow_back),
                                                                        onPressed: () => Navigator.of(context).pop(),
                                                                      ),
                                                                    ),
                                                                    Expanded(
                                                                      child: Text(
                                                                        article.libelle
                                                                            .toUpperCase(),
                                                                        textAlign: TextAlign.center,
                                                                        // Makes the title all caps for emphasis
                                                                        style: TextStyle(
                                                                          fontSize: 30,
                                                                          color: AppColor.primary,
                                                                          fontFamily: 'Varela',
                                                                          fontStyle:
                                                                          FontStyle
                                                                              .italic,
                                                                          // add italic style
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                          // add bold font weight
                                                                          letterSpacing:
                                                                          1.2,
                                                                          // add spacing between letters
                                                                          wordSpacing:
                                                                          2.0, // add spacing between words
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                Center(
                                                                  child: Container(
                                                                    width: MediaQuery.of(context).size.width * 0.6,
                                                                    // Adjust the width of the container to fit the image for most phones
                                                                    height: MediaQuery.of(context).size.height * 0.3,
                                                                    // Adjust the height of the container to fit the image for most phones
                                                                    child: ClipRRect(
                                                                      // Clips the image to a rounded rectangle for a more polished look
                                                                      borderRadius:
                                                                      BorderRadius.circular(6),
                                                                      child: Image.memory(
                                                                        article.imageBytes,
                                                                        fit: BoxFit.cover,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(width: 8),
                                                                // Adds some space between the image and the text
                                                                Row(
                                                                  children: [
                                                                    Expanded(
                                                                      child: Column(
                                                                        crossAxisAlignment:
                                                                        CrossAxisAlignment.start,
                                                                        children: [
                                                                          SizedBox(height: 14),
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                            children: [

                                                                              SizedBox(height: 14),
                                                                              Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                                children: [
                                                                                  Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      Text(
                                                                                        "PRIX",
                                                                                        style: TextStyle(
                                                                                          fontSize: 18,
                                                                                          color: Colors.grey[700],
                                                                                          fontWeight: FontWeight.bold,
                                                                                          letterSpacing: 1.2,
                                                                                        ),
                                                                                      ),
                                                                                      SizedBox(height: 4),
                                                                                      Text(
                                                                                        '${article.prix} Dinar',
                                                                                        style: TextStyle(
                                                                                          fontSize: 20,
                                                                                          color: AppColor.primary,
                                                                                          fontWeight: FontWeight.bold,
                                                                                          letterSpacing: 1.1,
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      Text(
                                                                                        "DURÉE",
                                                                                        style: TextStyle(
                                                                                          fontSize: 18,
                                                                                          color: Colors.grey[700],
                                                                                          fontWeight: FontWeight.bold,
                                                                                          letterSpacing: 1.2,
                                                                                        ),
                                                                                      ),
                                                                                      SizedBox(height: 4),
                                                                                      Text(
                                                                                        '${article.duree} Min',
                                                                                        style: TextStyle(
                                                                                          fontSize: 20,
                                                                                          color: AppColor.primary,
                                                                                          fontWeight: FontWeight.bold,
                                                                                          letterSpacing: 1.1,
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                              SizedBox(height: 12),
                                                                              Text(
                                                                                "DESCRIPTION",
                                                                                style: TextStyle(
                                                                                  fontSize: 18,
                                                                                  color: Colors.grey[700],
                                                                                  fontWeight: FontWeight.bold,
                                                                                  letterSpacing: 1.2,
                                                                                ),
                                                                              ),
                                                                              SizedBox(height: 4),
                                                                              Text(
                                                                                article.description,
                                                                                textAlign: TextAlign.center,
                                                                                style: TextStyle(
                                                                                  fontSize: 16,
                                                                                  color: AppColor.primary,
                                                                                  letterSpacing: 1.1,
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ), SizedBox(height: 8),
                                                                          Row(
                                                                            children: [
                                                                              Expanded(
                                                                                child: Divider(
                                                                                  color: Colors.grey,
                                                                                  height: 50,
                                                                                  thickness: 1,
                                                                                ),
                                                                              ),
                                                                              SizedBox(width: 16),
                                                                              InkWell(
                                                                                child: Icon(
                                                                                    Icons.favorite,
                                                                                    color: Colors.red,
                                                                                    size: 50
                                                                                ),
                                                                              ),
                                                                              SizedBox(width: 16),
                                                                              Expanded(
                                                                                child: Divider(
                                                                                  color: Colors.grey,
                                                                                  height: 50,
                                                                                  thickness: 1,
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          SizedBox(height: 10),
                                                                          Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center, // Updated
                                                                              children: [
                                                                                ElevatedButton.icon(
                                                                                  onPressed: () {
                                                                                    //addToCart(article);
                                                                                    final cartController = Get.find<ShoppingCartController>();
                                                                                    cartController.addItem(article, 1);
                                                                                    ScaffoldMessenger.of(context).showSnackBar(
                                                                                      SnackBar(
                                                                                        content: Text('${article.libelle} ajouté au panier'),
                                                                                        duration: Duration(seconds: 1),
                                                                                      ),
                                                                                    );
                                                                                  },
                                                                                  icon: Icon(Icons.shopping_cart,color: AppColor.secondary,),
                                                                                  label: Text(
                                                                                    'Ajouter au panier',
                                                                                    style: TextStyle(
                                                                                        fontSize: 16.0,
                                                                                        fontWeight: FontWeight.bold,
                                                                                        color: AppColor.secondary
                                                                                    ),
                                                                                  ),
                                                                                  style: ElevatedButton.styleFrom(
                                                                                    primary: AppColor.primary,
                                                                                    onPrimary: AppColor.primary,
                                                                                    shape: RoundedRectangleBorder(
                                                                                      borderRadius: BorderRadius.circular(10.0),
                                                                                    ),
                                                                                    elevation: 2.0,
                                                                                    padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
                                                                                  ),
                                                                                ),
                                                                              ]
                                                                          ),

                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  );
                                                },
                                                child: Container(
                                                  key: ValueKey(article.idArticle),





                                                  //TODO : hethi el card mte3 les articles eli ta7t les categorie
                                                  child: Card(
                                                    elevation: 4,
                                                    // Adds a shadow to the card for depth
                                                    child: Center(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                        // Aligns the text to the top of the card
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Expanded(
                                                                child: Text(
                                                                  article.libelle
                                                                      .toUpperCase(),
                                                                  textAlign: TextAlign.center,
                                                                  // Makes the title all caps for emphasis
                                                                  style: TextStyle(
                                                                    fontSize: 20,
                                                                    color: AppColor.primary,
                                                                    fontFamily: 'Varela',
                                                                    fontStyle:
                                                                    FontStyle
                                                                        .italic,
                                                                    // add italic style
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    // add bold font weight
                                                                    letterSpacing:
                                                                    1.2,
                                                                    // add spacing between letters
                                                                    wordSpacing:
                                                                    2.0, // add spacing between words
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Center(
                                                            child: Container(
                                                              width: MediaQuery.of(context).size.width * 0.5,
                                                              // Adjust the width of the container to fit the image for most phones
                                                              height: MediaQuery.of(context).size.height * 0.25,
                                                              // Adjust the height of the container to fit the image for most phones
                                                              child: ClipRRect(
                                                                // Clips the image to a rounded rectangle for a more polished look
                                                                borderRadius:
                                                                BorderRadius.circular(6),
                                                                child: Image.memory(
                                                                  article.imageBytes,
                                                                  fit: BoxFit.cover,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(width: 16),
                                                          // Adds some space between the image and the text
                                                          Row(
                                                            children: [
                                                              Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                  CrossAxisAlignment.start,
                                                                  children: [
                                                                    Center(
                                                          child: RatingBar.builder(
                                                          initialRating: articleController.calculateAverageRating(article),
                                                minRating: 1,
                                                direction: Axis.horizontal,
                                                allowHalfRating: true,
                                                itemCount: 5,
                                                itemSize: 20,
                                                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                                itemBuilder: (context, _) => Icon(
                                                  Icons.star,
                                                  color: Colors.amber,
                                                  size: 35,
                                                ),
                                                                        onRatingUpdate: (rating) async {
                                                                          print(rating);
                                                                          final Rating r1 = Rating(value: rating);
                                                                          final R1 = await ratingController.addRating(r1);
                                                                          print(R1?.id);
                                                                          article.ratings.add(R1!);
                                                                          await articleController.updateArticle(article);
                                                                          showDialog(
                                                                            context: context,
                                                                            builder: (BuildContext context) {
                                                                              // TODO : hethi el remerciement ki na3mel rating le stars
                                                                              return AlertDialog(
                                                                                backgroundColor: AppColor.primary,
                                                                                shape: RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.circular(16.0),
                                                                                ),
                                                                                title: Text(
                                                                                  "Merci beaucoup pour votre avis sur notre article de menu ${article.libelle}.",
                                                                                  style: TextStyle(
                                                                                    color : AppColor.secondary,
                                                                                    fontSize: 20.0,
                                                                                    fontWeight: FontWeight.bold,
                                                                                    fontStyle: FontStyle.italic,
                                                                                  ),
                                                                                ),
                                                                                content: Padding(
                                                                                  padding: EdgeInsets.symmetric(vertical: 8.0),
                                                                                  child: Text(
                                                                                    "Nous sommes ravis que vous ayez apprécié votre choix et nous espérons vous revoir bientôt chez nous.",
                                                                                    style: TextStyle(
                                                                                      color : AppColor.secondary,
                                                                                      fontSize: 16.0,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                actions: [
                                                                                  ElevatedButton(
                                                                                    style: ElevatedButton.styleFrom(
                                                                                      primary:   AppColor.primary,
                                                                                      shape: RoundedRectangleBorder(
                                                                                        borderRadius: BorderRadius.circular(8.0),
                                                                                      ),
                                                                                    ),
                                                                                    onPressed: () {
                                                                                      Navigator.pop(context);
                                                                                    },
                                                                                    child: Text(
                                                                                      'OK',
                                                                                      style: TextStyle(
                                                                                        color:   AppColor.secondary,
                                                                                        fontSize: 16.0,
                                                                                        fontWeight: FontWeight.bold,
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              );
                                                                            },
                                                                          );
                                                                        },
                                                                      ),
                                                                    ),
                                                                    SizedBox(height: 24),
                                                                    Column(
                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                      children: [
                                                                        SizedBox(height: 24),
                                                                        Row(
                                                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                          children: [
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: [
                                                                                Text(
                                                                                  "PRIX",
                                                                                  style: TextStyle(
                                                                                    fontSize: 14,
                                                                                    color: Colors.grey[700],
                                                                                    fontWeight: FontWeight.bold,
                                                                                    letterSpacing: 1.2,
                                                                                  ),
                                                                                ),
                                                                                SizedBox(height: 8),
                                                                                Text(
                                                                                  '${article.prix} Dinar',
                                                                                  style: TextStyle(
                                                                                    fontSize: 16,
                                                                                    color: AppColor.primary,
                                                                                    fontWeight: FontWeight.bold,
                                                                                    letterSpacing: 1.1,
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: [
                                                                                Text(
                                                                                  "DURÉE",
                                                                                  style: TextStyle(
                                                                                    fontSize: 14,
                                                                                    color: Colors.grey[700],
                                                                                    fontWeight: FontWeight.bold,
                                                                                    letterSpacing: 1.2,
                                                                                  ),
                                                                                ),
                                                                                SizedBox(height: 8),
                                                                                Text(
                                                                                  '${article.duree} Min',
                                                                                  style: TextStyle(
                                                                                    fontSize: 16,
                                                                                    color: AppColor.primary,
                                                                                    fontWeight: FontWeight.bold,
                                                                                    letterSpacing: 1.1,
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        SizedBox(height: 24),
                                                                        Text(
                                                                          "DESCRIPTION",
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            color: Colors.grey[700],
                                                                            fontWeight: FontWeight.bold,
                                                                            letterSpacing: 1.2,
                                                                          ),
                                                                        ),
                                                                        SizedBox(height: 8),
                                                                        Text(
                                                                          article.description,
                                                                          textAlign: TextAlign.center,
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            color: AppColor.primary,
                                                                            letterSpacing: 1.1,
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ), SizedBox(height: 8),
                                                                    Row(
                                                                      children: [
                                                                        Expanded(
                                                                          child: Divider(
                                                                            color: Colors.grey,
                                                                            height: 50,
                                                                            thickness: 1,
                                                                          ),
                                                                        ),
                                                                        SizedBox(width: 16),
                                                                        InkWell(
                                                                          child: Icon(
                                                                              Icons.favorite,
                                                                              color: Colors.red,
                                                                              size: 40
                                                                          ),
                                                                        ),
                                                                        SizedBox(width: 16),
                                                                        Expanded(
                                                                          child: Divider(
                                                                            color: Colors.grey,
                                                                            height: 50,
                                                                            thickness: 1,
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    SizedBox(height: 16),
                                                                    Row(
                                                                        mainAxisAlignment: MainAxisAlignment.center, // Updated
                                                                        children: [
                                                                          ElevatedButton.icon(
                                                                            onPressed: () {
                                                                              //addToCart(article);
                                                                              final cartController = Get.find<ShoppingCartController>();
                                                                              cartController.addItem(article, 1);
                                                                              ScaffoldMessenger.of(context).showSnackBar(
                                                                                SnackBar(
                                                                                  content: Text('${article.libelle} ajouté au panier'),
                                                                                  duration: Duration(seconds: 1),
                                                                                ),
                                                                              );
                                                                            },
                                                                            icon: Icon(Icons.shopping_cart,color: AppColor.secondary,),
                                                                            label: Text(
                                                                              'Ajouter au panier',
                                                                              style: TextStyle(
                                                                                  fontSize: 16.0,
                                                                                  fontWeight: FontWeight.bold,
                                                                                  color: AppColor.secondary
                                                                              ),
                                                                            ),
                                                                            style: ElevatedButton.styleFrom(
                                                                              primary: AppColor.primary,
                                                                              onPrimary: AppColor.primary,
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(10.0),
                                                                              ),
                                                                              elevation: 2.0,
                                                                              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
                                                                            ),
                                                                          ),
                                                                        ]
                                                                    ),
                                                                    SizedBox(height: 18),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                        ],
                                        //           )
                                        //       );
                                        //     },
                                        //   ),
                                        // ],
                                      ));
                                },
                              );
                            }
                          }),
                        ),
                    ],
                  ),
                ),
                  Positioned(
                    top: 35,
                    left: 20,
                    child: GestureDetector(
                      onTap: _signOut, // call the logout function when tapped
                      child: Image.asset(
                        'assets/images/logout.png', // replace with your image path
                        width: 50,
                        height: 50,
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Positioned(
                        bottom: 10,
                        right: 10,
                        child: GestureDetector(
                          onTap: () => launch('tel:29646006'),
                          child: Container(
                            decoration: BoxDecoration(
                              color: AppColor.primary,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.phone, color:  AppColor.secondary,),
                                SizedBox(width: 8),
                                Text(
                                  'Nous appeler',
                                  style: TextStyle(color: AppColor.secondary, fontSize: 16),
                                ),
                              ],
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    top: 30,
                    right: 10,
                    child: Obx(
                          () {
                        final cartItemCount = shoppingCartController.cartItems.length;
                        return cartItemCount == 0
                            ? Container()
                            : FloatingActionButton(
                          backgroundColor: Colors.white,
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ShoppingCartScreen()),
                            );
                          },
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.network(
                                'https://cdn.icon-icons.com/icons2/65/PNG/96/shoppingcart_accept_compra_12832.png',
                                width: 70,
                                height: 70,
                              ),
                              Positioned(
                                top: 4,
                                right: 4,
                                child: Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Text(
                                    cartItemCount.toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),




                ],
              ),
            ),
          ]),
    );
  }
}
