import 'package:flutter/material.dart';
import 'package:gestion_restaurantion/Screens/Utils/AppColor.dart';
import 'package:gestion_restaurantion/Screens/AuthScreen/LoginModal.dart';


class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/bg.jpg'), fit: BoxFit.cover)),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  padding: EdgeInsets.only(left: 16, right: 16, bottom: 32),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 60 / 100,
                  decoration: BoxDecoration(gradient: AppColor.linearBlackBottom),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(height: 40),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: 16),
                          // Log in Button
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            height: 60,
                            child: OutlinedButton(
                              child: Text('Connexion', style: TextStyle(color: AppColor.secondary, fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter')),
                              onPressed: () {
                                showModalBottomSheet(
                                  context: context,
                                  backgroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
                                  isScrollControlled: true,
                                  builder: (context) {
                                    return LoginModal();
                                  },
                                );
                              },
                              style: OutlinedButton.styleFrom(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                side: BorderSide(color: AppColor.secondary.withOpacity(0.5), width: 1),
                                primary: Colors.white,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            margin: EdgeInsets.only(top: 32),
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: 'En nous rejoignant, vous acceptez nos ',
                                style: TextStyle(color: Colors.white.withOpacity(0.6), height: 150 / 100),
                                children: [
                                  TextSpan(
                                    text: 'Conditions d\'utilisation ',
                                    style: TextStyle(color: Colors.white.withOpacity(0.6), fontWeight: FontWeight.w700, height: 150 / 100),
                                  ),
                                  TextSpan(
                                    text: 'et ',
                                    style: TextStyle(color: Colors.white.withOpacity(0.6), height: 150 / 100),
                                  ),
                                  TextSpan(
                                    text: 'Politique de confidentialité.',
                                    style: TextStyle(color: Colors.white.withOpacity(0.6), fontWeight: FontWeight.w700, height: 150 / 100),
                                  ),
                                ],
                              ),
                            ),
                          ),

                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
