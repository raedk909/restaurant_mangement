import 'package:flutter/material.dart';
import 'package:gestion_restaurantion/Controller/AuthenticationController/auth_Controller.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/UserController.dart';
// import 'package:hungry/views/screens/page_switcher.dart';
// import 'package:hungry/views/utils/AppColor.dart';
// import 'package:hungry/views/widgets/custom_text_field.dart';
import 'package:gestion_restaurantion/Screens/AuthScreen/WelcomePage.dart';
import 'package:gestion_restaurantion/Screens/Utils/AppColor.dart';
import 'package:gestion_restaurantion/Screens/AuthScreen/CustomTextField.dart';
import 'package:gestion_restaurantion/Screens/ClientScreen/MenuListScreen.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginModal extends StatefulWidget {
  @override
  State<LoginModal> createState() => _LoginModalState();
}
class _LoginModalState extends State<LoginModal> {

final AuthController authController = Get.find();
final UserController userController = Get.find();
final TextEditingController emailController = TextEditingController();
final TextEditingController passwordController = TextEditingController();

late SharedPreferences prefs;

void initState() {
  super.initState();
  SharedPreferences.getInstance().then((value) {
    setState(() {
      prefs = value;
    });
  });
}

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 85 / 100,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 32, top: 16),
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            physics: BouncingScrollPhysics(),
            children: [
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: MediaQuery.of(context).size.width * 35 / 100,
                  margin: EdgeInsets.only(bottom: 20),
                  height: 6,
                  decoration: BoxDecoration(color: Colors.grey[300], borderRadius: BorderRadius.circular(20)),
                ),
              ),
              // header
              Container(
                margin: EdgeInsets.only(bottom: 24),
                child: Text(
                  'Connexion',
                  style: TextStyle(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w700, fontFamily: 'inter'),
                ),
              ),
              // Form
              CustomTextField(title: 'Email', hint: 'email@gmail.com',controller: emailController,),
              CustomTextField(title: 'Mot de passe', hint: '**********', obsecureText: true, controller: passwordController,margin: EdgeInsets.only(top: 16)),
              // Log in Button
              Container(
                margin: EdgeInsets.only(top: 32, bottom: 6),
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: ElevatedButton(
                  onPressed: () async {
                    bool success = await authController.login(
                      emailController.text,
                      passwordController.text,
                    );
                    if (success) {
                      //todo ken success el email wel password sont stocké 3andi
                      await prefs.setString('email', emailController.text);
                      await prefs.setString('password', passwordController.text);
                      // Navigate to home screen
                      final user = await userController.loggedInUser ;
                      final  userx = await user ;
                      print(userx!.role);
                      if (userx != null && userx.role.name == "PERSONEL") {

                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MenuListScreen()),
                        );
                      }
                    }
                  },
                  child: Text('Connexion', style: TextStyle(color: AppColor.secondary, fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter')),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    primary: AppColor.primarySoft,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {},
                style: TextButton.styleFrom(
                  primary: Colors.white,
                ),
                child: RichText(
                  text: TextSpan(
                    text: 'Mot de passe oublié? ',
                    style: TextStyle(color: Colors.grey),
                    children: [
                      TextSpan(
                          style: TextStyle(
                            color: AppColor.primary,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'inter',
                          ),
                          text: 'Réinitialiser')
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
