// import 'package:gestion_restaurantion/Models/Article.dart';
//
// class SousCategories {
//   SousCategories({
//     required this.idSousCategorie,
//     required this.nom,
//     required this.articles,
//   });
//   late final int idSousCategorie;
//   late final String nom;
//   late final List<Articles> articles;
//
//   SousCategories.fromJson(Map<String, dynamic> json){
//     idSousCategorie = json['idSous_Categorie'];
//     nom = json['nom'];
//     articles = List.from(json['articles']).map((e)=>Articles.fromJson(e)).toList();
//   }
//
//   Map<String, dynamic> toJson() {
//     final _data = <String, dynamic>{};
//     _data['idSous_Categorie'] = idSousCategorie;
//     _data['nom'] = nom;
//     _data['articles'] = articles.map((e)=>e.toJson()).toList();
//     return _data;
//   }
// }