import 'dart:convert';
import 'dart:typed_data';

import 'package:gestion_restaurantion/Models/Article.dart';
import 'package:gestion_restaurantion/Models/sous_Categorie.dart';

class Categorie {
  Categorie({
    required this.idCategorie,
    required this.nom,
    required this.imageBytes,
    required this.articles
  });
  late final int idCategorie;
  late final String nom;
  late final Uint8List imageBytes;
  late final List<Articles> articles;

  Categorie.fromJson(Map<String, dynamic> json){
    idCategorie = json['idCategorie'];
    nom = json['nom'];
    imageBytes = json['image'] != null ? base64Decode(json['image']) : Uint8List(0);
    articles = List.from(json['articles']).map((e)=>Articles.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['idCategorie'] = idCategorie;
    _data['nom'] = nom;
    _data['image'] = base64Encode(imageBytes);
    _data['articles'] = articles.map((e)=>e.toJson()).toList();
    return _data;
  }
}
