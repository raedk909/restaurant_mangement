

import 'package:intl/intl.dart';

import 'Commande.dart';

class Facture {
  Facture({
    required this.idFacture,
    required this.dateFacture,
    required this.montantTotale,
    required this.TVA,
    required this.commande,
  });
  late final int idFacture;
  late final DateTime dateFacture;
  late final double montantTotale;
  late final double TVA;
  late final Commande commande;

  Facture.fromJson(Map<String, dynamic> json){
    idFacture = json['idFacture'];
    dateFacture = DateFormat('yyyy-MM-dd').parse(json['dateFacture']);
    montantTotale = json['montantTotale'];
    TVA = json['tva'];
    commande = Commande.fromJson(json['commande']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['idFacture'] = idFacture;
    _data['dateFacture'] = DateFormat('yyyy-MM-dd').format(dateFacture);
    _data['montantTotale'] = montantTotale;
    _data['tva'] = TVA;
    _data['commande'] = commande.toJson();
    return _data;
  }
}