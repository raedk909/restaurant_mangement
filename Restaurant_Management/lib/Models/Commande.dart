import 'package:gestion_restaurantion/Models/Client.dart';
import 'package:gestion_restaurantion/Models/TableRestaurant.dart';
import 'package:gestion_restaurantion/Models/ligneCommande.dart';
import 'package:intl/intl.dart';

class Commande {
  int idCommande;
  DateTime date;
  Etat etat;
  String description;
  List<ligneCommande> ligneCommandes;
  Client client;
  TableRestaurant tableRestaurant;

  Commande({
    required this.idCommande,
    required this.date,
    required this.etat,
    required this.description,
    required this.ligneCommandes,
    required this.client,
    required this.tableRestaurant,
  });

  factory Commande.fromJson(Map<String, dynamic> json) {
    return Commande(
      idCommande: json['idCommande'],
      date: DateFormat('yyyy-MM-dd').parse(json['date']),
      etat: _parseEtat(json['etat']),
      description: json['description'],
      ligneCommandes: (json['ligneCommandes'] as List)
          .map((data) => ligneCommande.fromJson(data))
          .toList(),
      client: Client.fromJson(json['client']),
      tableRestaurant: TableRestaurant.fromJson(json['tableRestaurant']),
    );
  }

  Map<String, dynamic> toJson() => {
    'idCommande': idCommande,
    'date': DateFormat('yyyy-MM-dd').format(date),
    'etat': _stringifyEtat(etat),
    'description': description,
    'ligneCommandes': ligneCommandes.map((e) => e.toJson()).toList(),
    'client': client.toJson(),
    'tableRestaurant': tableRestaurant.toJson(),
  };

  static Etat _parseEtat(String etatString) {
    switch (etatString) {
      case 'valide':
        return Etat.valide;
      case 'annule':
        return Etat.annule;
      case 'En_cours':
        return Etat.enCours;
      default:
        throw ArgumentError('Invalid Etat value: $etatString');
    }
  }

  static String _stringifyEtat(Etat etat) {
    switch (etat) {
      case Etat.valide:
        return 'valide';
      case Etat.annule:
        return 'annule';
      case Etat.enCours:
        return 'En_cours';
      default:
        throw ArgumentError('Invalid Etat value: $etat');
    }
  }
  double get prixTotale {
    double total = 0;
    for (var ligneCommande in ligneCommandes) {
      total += ligneCommande.prixLigne;
    }
    return total;
  }
}

enum Etat { valide, annule, enCours }














