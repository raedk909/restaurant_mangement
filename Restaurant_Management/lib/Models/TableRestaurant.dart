class TableRestaurant {
  TableRestaurant({
    required this.idTable,
    required this.numero,
  });
  late final int idTable;
  late final int numero;

  TableRestaurant.fromJson(Map<String, dynamic> json){
    idTable = json['idTable'];
    numero = json['numero'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['idTable'] = idTable;
    _data['numero'] = numero;
    return _data;
  }
}