import 'package:gestion_restaurantion/Models/User.dart';

class Client {
  Client({
    required this.id,
    required this.firstname,
    required this.lastname,
    required this.dateNais,
    required this.numero,
    required this.user,
  });
  late final int id;
  late final String firstname;
  late final String lastname;
  late final String dateNais;
  late final int numero;
  late final User user;

  Client.fromJson(Map<String, dynamic> json){
    id = json['id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    dateNais = json['dateNais'];
    numero = json['numero'];
    user = User.fromJson(json['user']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['firstname'] = firstname;
    _data['lastname'] = lastname;
    _data['dateNais'] = dateNais;
    _data['numero'] = numero;
    _data['user'] = user.toJson();
    return _data;
  }
}