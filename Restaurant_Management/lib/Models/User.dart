import 'package:flutter/foundation.dart';


  // User({
  //   required this.idUser,
  //   required this.email,
  //   required this.password,
  //   required this.role,
  // });
  // late final int idUser;
  // late final String email;
  // late final String password;
  // late final String role;
  //
  // User.fromJson(Map<String, dynamic> json){
  //   idUser = json['idUser'];
  //   email = json['email'];
  //   password = json['password'];
  //   role = json['role'];
  // }
  //
  // Map<String, dynamic> toJson() {
  //   final _data = <String, dynamic>{};
  //   _data['idUser'] = idUser;
  //   _data['email'] = email;
  //   _data['password'] = password;
  //   _data['role'] = role;
  //   return _data;
  // }
  // bool get isUser => role == 'USER';

enum UserRole {
  USER,
  PERSONEL,
  ADMIN,
}

class User {
  User({
    required this.idUser,
    required this.email,
    required this.password,
    required this.role,
  });

  late final int idUser;
  late final String email;
  late final String password;
  late final UserRole role;

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      idUser: json['idUser'],
      email: json['email'],
      password: json['password'],
      role: _parseEtat(json['role']),
    );
  }

  Map<String, dynamic> toJson() => {
    'idUser': idUser,
    'email': email,
    'password': password,
    'role': _stringifyEtat(role)
  };

  static UserRole _parseEtat(String etatString) {
    switch (etatString) {
      case 'USER':
        return UserRole.USER;
      case 'ADMIN':
        return UserRole.ADMIN;
      case 'PERSONEL':
        return UserRole.PERSONEL;
      default:
        throw ArgumentError('Invalid role value: $etatString');
    }
  }

  static String _stringifyEtat(UserRole role) {
    switch (role) {
      case UserRole.USER:
        return 'USER';
      case UserRole.ADMIN:
        return 'ADMIN';
      case UserRole.PERSONEL:
        return 'PERSONEL';
      default:
        throw ArgumentError('Invalid role value: $role');
    }
  }
}


