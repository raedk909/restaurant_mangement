import 'package:gestion_restaurantion/Models/Article.dart';

class ligneCommande {
  ligneCommande({
    required this.idLigneCommande,
    required this.prixLigne,
    required this.quantite,
    required this.article,
  });
  late final int idLigneCommande;
  late  double prixLigne;
  late  int quantite;
  late final Articles article;

  ligneCommande.fromJson(Map<String, dynamic> json){
    idLigneCommande = json['idLigneCommande'];
    prixLigne = json['prixLigne'];
    quantite = json['quantite'];
    article = Articles.fromJson(json['article']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['idLigneCommande'] = idLigneCommande;
    _data['prixLigne'] = prixLigne;
    _data['quantite'] = quantite;
    _data['article'] = article.toJson();
    return _data;
  }
}