import 'dart:convert';
import 'dart:typed_data';

class Articles {
  Articles({
    required this.idArticle,
    required this.description,
    required this.libelle,
    required this.prix,
    required this.reference,
    required this.duree,
    required this.imageBytes,
    required this.ratings,
  });

  late final int idArticle;
  late final String description;
  late final String libelle;
  late final double prix;
  late final String reference;
  late final String duree ;
  late final Uint8List imageBytes;
  late final List<Rating> ratings;

  Articles.fromJson(Map<String, dynamic> json)
      : idArticle = json['idArticle'],
        description = json['description'],
        libelle = json['libelle'],
        prix = json['prix'],
        reference = json['reference'],
        duree = json['duree'],
        imageBytes = json['image'] != null
            ? base64Decode(json['image'])
            : Uint8List(0),
        ratings = List.from(json['ratings']).map((e)=>Rating.fromJson(e)).toList();

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['idArticle'] = idArticle;
    _data['description'] = description;
    _data['libelle'] = libelle;
    _data['prix'] = prix;
    _data['reference'] = reference;
    _data['duree'] = duree;
    _data['image'] = base64Encode(imageBytes);
    _data['ratings'] = ratings.map((e)=>e.toJson()).toList();
    return _data;
  }

}
class Rating {
  Rating({
    this.id,
    required this.value,
  });
  late final int? id;
  late final double value;

  Rating.fromJson(Map<String, dynamic> json){
    id = json['id'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['value'] = value;
    return _data;
  }
}