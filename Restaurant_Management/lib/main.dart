import 'package:flutter/material.dart';
import 'package:gestion_restaurantion/Controller/AuthenticationController/auth_Controller.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/ClientController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/CommandeController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/TableRestaurantController.dart';
import 'package:gestion_restaurantion/Controller/ModelsController/UserController.dart';
import 'package:get/get.dart';

import 'package:gestion_restaurantion/Screens/AuthScreen/WelcomePage.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //Get.lazyPut(() => CategorieController());
    Get.lazyPut(() => AuthController());
    Get.lazyPut(() => TableRestaurantController());
    Get.lazyPut(() => UserController());
    Get.lazyPut(() => ClientController());
    Get.lazyPut(() => CommandeController());
    return GetMaterialApp(
      routes: {
      },
      debugShowCheckedModeBanner: false,
      title: 'my app',
      home: //CustomSearchBar()
      WelcomePage()
      ,
    );
  }
}